import h5py
import numpy as np

na = 1016
np.random.seed(142)
name = "dynmat.hdf5"
test = False

with h5py.File(name, "w") as f:
    dset = f.create_dataset("test1", (na, na), dtype="float64")
    # dset[...] = np.diag(np.arange(na))
    matrix = np.random.rand(na, na).astype(np.float64)
    msym = matrix + matrix.T
    dset[...] = msym
    if test:
        v = np.linalg.eigvalsh(msym)
        np.savetxt("ref_eigval.dat", v)
